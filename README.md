California Pools is one of the largest pool builders in the country and brings award-winning custom pools to the Claremont area. As seen on Pool Kings, we offer expert design solutions and innovative construction techniques to provide our customers with the highest-quality pool or backyard anywhere.

Address: 206 W Bonita Ave, #2P, Claremont, CA 91711, USA

Phone: 909-419-3621

Website: https://californiapools.com/locations/claremont